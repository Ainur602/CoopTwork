package ru.itis.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.itis.forms.EditProfileForm;
import ru.itis.model.Profile;
import ru.itis.model.User;
import ru.itis.repository.UserRepository;

import java.util.Optional;

@Component
public class ProfileEditFormValidator implements Validator {

    private final UserRepository usersRepository;

    @Autowired
    public ProfileEditFormValidator(UserRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(EditProfileForm.class.getName());
    }

    @Override
    public void validate(Object target, Errors errors) {
        EditProfileForm profileForm = (EditProfileForm) target;
        Optional<User> existedUserByLogin = usersRepository.findOneByLogin(profileForm.getName());
        Optional<User> existedUserByEmail = usersRepository.findOneByEmail(profileForm.getSurname());

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "empty.name", "Please enter your name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "empty.surname", "Please enter your surname");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthday", "empty.birthday", "Please enter your birthday");

    }
}
