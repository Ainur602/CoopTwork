package ru.itis.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.model.User;
import ru.itis.repository.UserRepository;

import java.util.Optional;
import java.util.regex.Pattern;

@Component
public class UserRegistrationFormValidator implements Validator {

    private final UserRepository usersRepository;

    @Autowired
    public UserRegistrationFormValidator(UserRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(UserRegistrationForm.class.getName());
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserRegistrationForm form = (UserRegistrationForm)target;
        Optional<User> existedUserByLogin = usersRepository.findOneByLogin(form.getLogin());
        Optional<User> existedUserByEmail = usersRepository.findOneByEmail(form.getEmail());

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "empty.login", "Пустой логин");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "empty.email", "Пустой email");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pass", "empty.password", "Пустой пароль");

        if (existedUserByLogin.isPresent()) {
            errors.reject("bad.login", "This login already is used");
        }
        if (!Pattern.matches("[a-zA-Z]{4,20}" , form.getLogin())) {
            errors.reject("bad.login", "Incorrect login");
        }
        if (!Pattern.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$" , form.getEmail())) {
            errors.reject("bad.login", "Incorrect email address");
        }
        if (existedUserByEmail.isPresent()) {
            errors.reject("bad.email", "This email already is used");
        }
        if (!form.getPass().equals(form.getPassCon())){
            errors.reject("bad.password", "Password are not the same");
        }
    }
}
