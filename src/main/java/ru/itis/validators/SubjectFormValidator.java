package ru.itis.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.itis.forms.EditProfileForm;
import ru.itis.forms.SubjectForm;
import ru.itis.model.User;
import ru.itis.repository.SubjectRepository;
import ru.itis.repository.UserRepository;

import java.util.Optional;

@Component
public class SubjectFormValidator implements Validator{

  private final SubjectRepository subjectRepository;

  @Autowired
  public SubjectFormValidator(SubjectRepository subjectRepository) {
    this.subjectRepository = subjectRepository;
  }

  @Override
  public boolean supports(Class<?> aClass) {
    return aClass.getName().equals(SubjectForm.class.getName());
  }

  @Override
  public void validate(Object target, Errors errors) {
    SubjectForm subjectForm = (SubjectForm) target;

    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "empty.name", "Please enter name of subject");
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "image", "empty.image", "Please choose image to subject ");

  }
}
