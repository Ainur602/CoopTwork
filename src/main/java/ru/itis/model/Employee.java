package ru.itis.model;

import lombok.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NaturalIdCache;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Employee")
@Table(name = "employees")
@ToString(exclude = {"user", "groups"})
public class Employee {
    @Id
    private Long id;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    private User user;


    @Builder.Default
    @OneToMany(mappedBy = "employee", fetch = FetchType.EAGER,
            cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE})
    private List<GrEmpAssociation> groups = new ArrayList<>();
}
