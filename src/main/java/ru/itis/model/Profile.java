package ru.itis.model;

import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Table(name = "profiles")
@Entity(name = "Profile")
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = {"user","subjects", "avatar"})
@Builder
@Setter
@Getter
public class Profile {
    @Id
    private Long id;

    @MapsId // This way, the id column serves as both Primary Key and FK
    @OneToOne(fetch = FetchType.LAZY)
    private User user;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "avatar_id")
    private File avatar;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "about", length = 2000)
    private String about;

    @Column(name = "birthday")
    //@Temporal(TemporalType.DATE)
    private String birthday;

    // for ManyToMany to Subject
    @Builder.Default
    @OneToMany(mappedBy = "profile", fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE})
    private List<SubjectAssociation> subjects = new ArrayList<>();

    @Builder.Default
    @OneToMany(fetch = FetchType.LAZY , mappedBy = "author", cascade=CascadeType.ALL)
    private List<Comment> comments = new ArrayList<>();

    @Builder.Default
    @OneToMany(fetch = FetchType.LAZY , mappedBy = "profile", cascade=CascadeType.ALL)
    private List<Notification> notifications = new ArrayList<>();
}
