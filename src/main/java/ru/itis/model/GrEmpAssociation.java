package ru.itis.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "group_emp_association")
@Setter
@Getter
@ToString(exclude = {"group", "employee"})
public class GrEmpAssociation {

    @EmbeddedId
    private GrEmpAssociationPK id = new GrEmpAssociationPK();

    @ManyToOne(cascade =
            {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.MERGE, CascadeType.DETACH})
    @MapsId("groupId")
    @JoinColumn(name = "groupId")
    private Group group;

    @ManyToOne(cascade =
            {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.MERGE, CascadeType.DETACH})
    @MapsId("employeeId")
    @JoinColumn(name = "employeeId")
    private Employee employee;

    @Column(name = "isAccepted",columnDefinition = "boolean default false")
    private Boolean isAccepted = Boolean.FALSE;
}