package ru.itis.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class GrEmpAssociationPK implements Serializable{
    @Column(name = "groupId")
    private Long groupId;

    @Column(name = "employeeId")
    private Long employeeId;
}
