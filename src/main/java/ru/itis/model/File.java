package ru.itis.model;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "files")
@ToString(exclude = {"profile", "subject","group", "notification"})
public class File {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @OneToOne(mappedBy = "avatar")
  private Profile profile;

  @OneToOne(mappedBy = "image")
  private Subject subject;

  @OneToOne(mappedBy = "image")
  private Group group;

  @OneToOne(mappedBy = "image")
  private Notification notification;

  private String storageFileName;

  private String originalFileName;

  private long size;

  private String type;

  private String url;
}
