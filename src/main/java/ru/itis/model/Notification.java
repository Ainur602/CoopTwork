package ru.itis.model;


import lombok.*;
import ru.itis.utils.LocalDateTimeConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "notification")
@Entity(name = "Notification")
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = {"profile", "image"})
@Builder
@Setter
@Getter
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "text", length = 1000)
    private String text;

    @Column(name = "isChecked",columnDefinition = "boolean default false")
    private Boolean isChecked = Boolean.FALSE;

    @Column(name = "isInvite",columnDefinition = "boolean default false")
    private Boolean isInvite = Boolean.FALSE;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.DETACH})
    @JoinColumn(name = "profile_id")
    private Profile profile;

    @Convert(converter = LocalDateTimeConverter.class)
    private LocalDateTime addedDate;

    @Column(name = "group_id")
    private Long groupId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "image_id")
    private File image;
}
