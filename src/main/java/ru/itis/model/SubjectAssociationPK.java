package ru.itis.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@EqualsAndHashCode
public class SubjectAssociationPK implements Serializable{

    @Column(name = "profileId")
    private Long profileId;

    @Column(name = "subjectId")
    private Long subjectId;

}
