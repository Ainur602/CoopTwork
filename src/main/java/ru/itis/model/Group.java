package ru.itis.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "Group")
@Table(name = "groups")
@ToString(exclude = {"employer", "image", "employees", "comments"})
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "image_id")
    private File image;

    @Column(name = "about")
    private String about;

    @Column(name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    private Employer employer;

    // for ManyToMany to Subject
    @Builder.Default
    @OneToMany(mappedBy = "group", fetch = FetchType.EAGER, orphanRemoval = true,
            cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE})
    private List<GrEmpAssociation> employees = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "group", cascade = CascadeType.ALL)
    private List<Comment> comments;
}
