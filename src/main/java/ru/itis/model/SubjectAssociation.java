package ru.itis.model;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Entity
@Table(name = "subject_association")
@Setter
@Getter
@ToString(exclude = {"profile", "subject"})
public class SubjectAssociation {

    @EmbeddedId
    private SubjectAssociationPK id = new SubjectAssociationPK();

    @ManyToOne(cascade =
            {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.MERGE})
    @MapsId("profileId")
    @JoinColumn(name = "profileId")
    private Profile profile;

    @ManyToOne(cascade =
            {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.MERGE})

    @MapsId("subjectId")
    @JoinColumn(name = "subjectId")
    private Subject subject;

    @Max(value = 5)
    @Min(value = 0)
    @Column(name = "rating",columnDefinition = "int default 0")
    private int rating = 0;


}
