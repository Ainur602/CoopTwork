package ru.itis.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Employer")
@Table(name = "employers")
@ToString(exclude = "user")
@Builder
public class Employer {
    @Id
    private Long id;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    private User user;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,
            mappedBy = "employer", orphanRemoval = true)
    @Builder.Default
    private List<Group> groups = new ArrayList<>();
}
