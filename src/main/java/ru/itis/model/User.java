package ru.itis.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import ru.itis.security.roles.Role;
import ru.itis.security.states.State;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Table(name = "users")
@Entity(name = "User")
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = {"profile", "employee", "employer"})
@Builder
@Setter
@Getter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "login", unique = true)
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Enumerated(EnumType.STRING)
    private State state;

    @OneToOne(mappedBy = "user",fetch = FetchType.LAZY,
            cascade = CascadeType.ALL, orphanRemoval = true, optional = false)
    private Profile profile;

    @OneToOne(mappedBy = "user",fetch = FetchType.LAZY,
            cascade = CascadeType.ALL, orphanRemoval = true, optional = false)
    private Employee employee;

    @OneToOne(mappedBy = "user",fetch = FetchType.LAZY,
            cascade = CascadeType.ALL, orphanRemoval = true, optional = false)
    private Employer employer;

    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "uuid")
    private UUID uuid;
}
