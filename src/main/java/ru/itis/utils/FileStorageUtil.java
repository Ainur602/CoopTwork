package ru.itis.utils;

import lombok.SneakyThrows;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.model.File;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.UUID;

@Component
public class FileStorageUtil {

  @Value("${storage.path}")
  private String storagePath;

  @Value("${url.path}")
  private String urlPath;

  public String getStoragePath() {
    return storagePath;
  }

  @SneakyThrows
  public void copyToStorage(MultipartFile file, String storageFileName) {
    Files.copy(file.getInputStream(), Paths.get(storagePath, storageFileName));
  }

  public File convertFromMultipart(MultipartFile file) {
    // получаем название файла
    String originalFileName = file.getOriginalFilename();
    // вытаскиваем контент-тайп
    String type = file.getContentType();
    // размер файла
    long size = file.getSize();
    // создаем имя файла
    String storageName = createStorageName(originalFileName);
    // получаем urlPath файла по которому он будет доступен внутри системы
    String fileUrl = getUrlOfFile(storageName);
    return File.builder()
            .originalFileName(originalFileName)
            .storageFileName(storageName)
            .url(fileUrl)
            .size(size)
            .type(type)
            .build();
  }

  private String getUrlOfFile(String storageFileName) {
    return urlPath + "\\" + storageFileName;
  }

  private String createStorageName(String originalFileName) {
    String extension = FilenameUtils.getExtension(originalFileName);
    String newFileName = UUID.randomUUID().toString();
    return newFileName + "." + extension;
  }

  public boolean deleteFromStorage(String storageFileName) {
//      java.io.File file = new java.io.File(storagePath+storageFileName);
    Path filePath = Paths.get(storagePath + storageFileName);
    try {
      Files.delete(filePath);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return true;
  }
}
