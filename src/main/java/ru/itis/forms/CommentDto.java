package ru.itis.forms;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentDto {
    private Long userId;
    private Long groupId;
    private String text;
}
