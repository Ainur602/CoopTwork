package ru.itis.forms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.itis.model.File;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SubjectCardDto {
    private Long subjectId;
    private Boolean isChecked;
    private String name;
    private Integer rating;
    private String description;
    private File image;
}
