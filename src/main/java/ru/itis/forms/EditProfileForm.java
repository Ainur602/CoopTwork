package ru.itis.forms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EditProfileForm {
  private MultipartFile avatar;
  private String name;
  private String surname;
  private String about;
  private String birthday;
}
