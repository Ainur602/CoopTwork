package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.itis.forms.SubjectForm;
import ru.itis.model.Subject;
import ru.itis.model.User;
import ru.itis.services.interfaces.GroupService;
import ru.itis.services.interfaces.SubjectService;
import ru.itis.services.interfaces.UserService;
import ru.itis.validators.SubjectFormValidator;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value = "/admin/dashboard")
public class AdminController {

    private final UserService userService;
    private final SubjectService subjectService;
    private final GroupService groupService;
    private final SubjectFormValidator subjectFormValidator;

    @Autowired
    public AdminController(UserService userService, SubjectService subjectService, GroupService groupService, SubjectFormValidator subjectFormValidator) {
        this.userService = userService;
        this.subjectService = subjectService;
        this.groupService = groupService;
        this.subjectFormValidator = subjectFormValidator;
    }

    @InitBinder("subjectForm")
    public void initProfileEditFormValidator(WebDataBinder binder){
        binder.addValidators(subjectFormValidator);
    }


    @GetMapping("/users")
    public String getUsersDashboardPage(@ModelAttribute("model")ModelMap model){
        List<User> userList = userService.findAll();
        List<Subject> subjectList = subjectService.findAll();

        model.addAttribute("subjectListSize", subjectList.size());
        model.addAttribute("userList", userList);
        model.addAttribute("userListSize", userList.size());
        model.addAttribute("groupListSize", groupService.findAll().size());
        return "admin-dashboard-users";
    }

    @GetMapping("/subjects")
    public String getTagsDashboardPage(@ModelAttribute("model")ModelMap model){
        List<User> userList = userService.findAll();
        List<Subject> subjectList = subjectService.findAll();

        model.addAttribute("subjectList", subjectList);
        model.addAttribute("subjectListSize", subjectList.size());
        model.addAttribute("userListSize", userList.size());
        model.addAttribute("groupListSize", groupService.findAll().size());
        return "admin-dashboard-subjects";
    }


    @GetMapping("/subjects/create")
    public String getCreateTagPage(){
        return "admin-add-subject";
    }

    @PostMapping("/subjects/create")
    public String postCreateTagPage(@Valid @ModelAttribute("subjectForm")SubjectForm subjectForm, BindingResult bindingResult,
                                    RedirectAttributes attributes){

        if (bindingResult.hasErrors()){
            attributes.addFlashAttribute("error", bindingResult.getAllErrors().get(0).getDefaultMessage());
            return "redirect:create";
        }
        // save Subject
        String subjectName = subjectService.save(subjectForm);
        // create message to user
        attributes.addFlashAttribute("message", "The subject <" + subjectName + "> successfully created");

        return "redirect:/admin/dashboard/subjects";
    }

    @GetMapping("/subjects/delete/{subject-id}")
    public String deleteSubject(@PathVariable("subject-id") String subjectId, RedirectAttributes attributes){
        Subject subject = subjectService.findOneById(subjectId);
        String subjectName = subjectService.delete(subject);

        attributes.addFlashAttribute("message", "The subject <" + subjectName + "> successfully deleted");
        return "redirect:/admin/dashboard/subjects";
    }
}
