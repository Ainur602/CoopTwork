package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.forms.CommentDto;
import ru.itis.services.interfaces.CommentService;

@Controller
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("/comments/add")
    public String addComment(@ModelAttribute CommentDto commentDto){
        commentService.save(commentDto);
        return "redirect:/groups/group" +commentDto.getGroupId();
    }
}
