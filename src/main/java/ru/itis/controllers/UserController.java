package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.itis.forms.EditProfileForm;
import ru.itis.services.interfaces.AuthenticationService;
import ru.itis.services.interfaces.UserService;
import ru.itis.validators.ProfileEditFormValidator;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/user")
public class UserController {

    private final AuthenticationService authService;
    private final UserService userService;
    private final ProfileEditFormValidator profileEditFormValidator;

    @Autowired
    public UserController(AuthenticationService authService, ProfileEditFormValidator profileEditFormValidator, UserService userService) {
        this.authService = authService;
        this.profileEditFormValidator = profileEditFormValidator;
        this.userService = userService;
    }

    @InitBinder("profileForm")
    public void initProfileEditFormValidator(WebDataBinder binder){
        binder.addValidators(profileEditFormValidator);
    }

    @GetMapping("/id{user-id}")
    public String getProfilePage(@PathVariable("user-id") String userId, @ModelAttribute("model") ModelMap model) {

        model.addAttribute("user", userService.getUserById(userId));
        return "profile";
    }

    @GetMapping("/profile/edit")
    public String getEditProfilePage(){
        return "profile-edit";
    }


    @PostMapping("/profile/edit")
    public String postEditProfilePage(@Valid @ModelAttribute("profileForm") EditProfileForm profileFrom, BindingResult bindingResult,
                                      RedirectAttributes attributes, Authentication authentication){
        if (bindingResult.hasErrors()){
            attributes.addFlashAttribute("error", bindingResult.getAllErrors().get(0).getDefaultMessage());
            return "redirect:edit";
        }
        userService.updateProfile(authentication, profileFrom);
        return "redirect:/";
    }
}
