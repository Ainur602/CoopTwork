package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.itis.forms.GroupForm;
import ru.itis.model.Employee;
import ru.itis.model.Group;
import ru.itis.model.User;
import ru.itis.services.interfaces.*;

import java.util.List;

@Controller
public class GroupController {

    private final GroupService groupService;
    private final AuthenticationService authenticationService;
    private final UserService userService;
    private final EmpGroupAssociationService empGroupAssociationService;
    private final NotificationService notificationService;
    
    @Autowired
    public GroupController(GroupService groupService, AuthenticationService authenticationService, UserService userService, EmpGroupAssociationService empGroupAssociationService, NotificationService notificationService) {
        this.groupService = groupService;
        this.authenticationService = authenticationService;
        this.userService = userService;
        this.empGroupAssociationService = empGroupAssociationService;
        this.notificationService = notificationService;
    }

    // TODO getAllUserGroups by Authentication
    @GetMapping("/admin/groups")
    public String getAllGroups(@ModelAttribute("model") ModelMap modelMap){
        modelMap.addAttribute("groupList", groupService.findAll());
        return "groups";
    }




    /**
     * @return created Groups created by User
     */
    @GetMapping("/user{user-id}/groups")
    public String getCreatedGroupsByUser(@ModelAttribute("model") ModelMap modelMap,@PathVariable("user-id") String userId, Authentication authentication){
        String authUserId = authenticationService.getUserByAuthentication(authentication).getId().toString();
        if (authUserId.equals(userId)) {
            List<Group> groupList = groupService.getAllCreatedGroupsByUser(userId);
            modelMap.addAttribute("groupList", groupList);
            return "groups";
        }else {
            return "redirect:/user" + authUserId + "/groups";
        }
    }

    /**
     * @return Groups where User is member
     */
    @GetMapping("/user{user-id}/groups/member")
    public String getGroupsAsEmployee(@ModelAttribute("model") ModelMap modelMap,@PathVariable("user-id") String userId, Authentication authentication){
        String authUserId = authenticationService.getUserByAuthentication(authentication).getId().toString();
        if (authUserId.equals(userId)) {
            List<Group> groupList = groupService.getAllGroupWhereUserIsEmployee(userId);
            modelMap.addAttribute("groupList", groupList);
            return "groups";
        } else {
            return "redirect:/user" + authUserId + "/groups/member";
        }
    }

    /**
     * @return list of Group`s Employees (Members)
     */
    @GetMapping("/groups/group{group-id}/members")
    public String getGroupEmployeesPage(Model model, @PathVariable("group-id") String groupId){
        Group group = groupService.getGroupById(groupId);
        model.addAttribute("group", group);
        return "group-members";
    }

    /**
     * @param login RequestParam for Search
     * @param groupId Need to invite to that group
     * @return page to invite to the Group with User`s with login like :login
     */
    @GetMapping("/groups/group{group-id}/members/search")
    public String searchMembers(Model model, @RequestParam(value = "login", required = false) String login, @PathVariable("group-id") String groupId) {
        Group group = groupService.getGroupById(groupId);

        //List<User> userList = userService.findByLoginContaining(login);
        List<User> userList = userService.findByLoginContaining(group, login);
        model.addAttribute("group", group);
        model.addAttribute("userList", userList);
        return "group-members-search";
    }

    /**
     * We can get that page only if you created Group or you are an Employee
     * @return Group page
     */
    @GetMapping("/groups/group{group-id}")
    public String getGroupPagePage(Model model, @PathVariable("group-id") String groupId, Authentication authentication){
        User user = authenticationService.getUserByAuthentication(authentication);
        Group group  = groupService.getGroupById(groupId);

        if (groupService.hasUserAccessToGroup(user, group)){
            model.addAttribute("group", group);
            // Size of members
            model.addAttribute("membersSize",groupService.getAcceptedGroupMembers(group).size()+1);
            return "group-page";
        } else {
            return "redirect:/";
        }
    }

    /**
     * @return page to create Group
     */
    @GetMapping("/groups/create")
    public String getCreateGroupPage(){
        return "create-group";
    }

    /**
     * Save Group Entity
     * @param groupForm DTO
     * @param authentication to set author
     * @return redirect to created Group
     */
    @PostMapping("/groups/create")
    public String postCreateGroup(@ModelAttribute GroupForm groupForm, Authentication authentication){
        User user = authenticationService.getUserByAuthentication(authentication);
        Long groupId = groupService.save(user, groupForm);
        return "redirect:/groups/group" + groupId;
    }

    /**
     * Edit GroupPage // Get
     */
    @GetMapping("/groups/group{group-id}/edit")
    public String getEditGroup(Model model, @PathVariable("group-id") String groupId, Authentication authentication){
        User user = authenticationService.getUserByAuthentication(authentication);
        Group group = groupService.getGroupById(groupId);

        // if authenticated user is author of the group
        if (group.getEmployer().getId().equals(user.getId())){
            model.addAttribute("group", group);
            return "edit-group";
        }
        return "redirect:/user" + user.getId() + "/groups";
    }

    /**
     * Edit GroupPage // Post
     */
    @PostMapping("/groups/group{group-id}/edit")
    public String postEditGroup(@ModelAttribute GroupForm groupForm, @PathVariable("group-id") String groupId, Authentication authentication, Model model){
        Group group =groupService.getGroupById(groupId);
        groupService.update(group, groupForm);
        model.addAttribute("group", group);
        return "redirect:/groups/group" + groupId;
    }

    /**
     * Edit GroupPage // Post
     */
    @PostMapping("/groups/delete")
    public String postDeleteGroup(@RequestParam("groupId")String groupId, Authentication authentication){
        Group group = groupService.getGroupById(groupId);
        User user = authenticationService.getUserByAuthentication(authentication);

        if (group.getEmployer().getId().equals(user.getId())){
            groupService.delete(group);
        }
        return "redirect:/user" + user.getId() + "/groups";
    }

    /**
     * Employer Invite Employee to the Group (Save GrEmpAssociation)
     * @param  employeeId has the same Id as User
     */
    @PostMapping("/groups/group{group-id}/members/add")
    public String postGroupEmployeesAdd(@RequestParam("employeeId") String employeeId ,@PathVariable("group-id") String groupId){
        Group group = groupService.getGroupById(groupId);
        Employee employee = userService.getUserById(employeeId).getEmployee();
        empGroupAssociationService.inviteEmployeeToGroup(group, employee);
        return "redirect:/groups/group" + groupId + "/members/";
    }

    /**
     * remove User from Group
     */
    @PostMapping("/groups/group{group-id}/members/remove")
    public String postGroupEmployeesDelete(@RequestParam("employeeId") String employeeId ,@PathVariable("group-id") String groupId){
        Group group = groupService.getGroupById(groupId);
        Employee employee = userService.getUserById(employeeId).getEmployee();
        Boolean flag = empGroupAssociationService.deleteUserFromGroup(group, employee);
        if (flag){
            return "redirect:/groups/group" +  groupId +"/members";
        }
        else return "redirect:/user" + employeeId + "/groups/member";
    }

    /**
     * Employee accept invitation to the Group (set in Association isAccepted true)
     */
    @PostMapping("/groups/group{group-id}/join")
    public String postJoinToGroup(@RequestParam("employeeId") String employeeId,@RequestParam("inviteId") String inviteId, @PathVariable("group-id") String groupId){
        empGroupAssociationService.joinToGroup(Long.valueOf(groupId), Long.valueOf(employeeId));
        notificationService.deleteNotification(inviteId);
        return "redirect:/groups/group" + groupId;
    }


    /**
     * Employee reject invitation to the Group,
     * then GroupEmployeeAssociation get removed and Group Creator get notification about that
     */
    @PostMapping("/groups/group{group-id}/reject")
    public String postRejectInviteToGroup(@RequestParam("employeeId") String employeeId,@RequestParam("inviteId") String inviteId, @PathVariable("group-id") String groupId){
        Group group = groupService.getGroupById(groupId);
        Employee employee = userService.getUserById(employeeId).getEmployee();
        empGroupAssociationService.deleteUserFromGroup(group, employee);
        notificationService.notifyEmployerAboutRejection(group, employee);
        notificationService.deleteNotification(inviteId);
        return "redirect:/user/id" + employeeId + "/notifications";
    }
}
