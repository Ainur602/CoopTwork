package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.itis.forms.SubjectCardDtoWrapper;
import ru.itis.model.Subject;
import ru.itis.model.SubjectAssociation;
import ru.itis.model.User;
import ru.itis.services.interfaces.SubjectService;
import ru.itis.services.interfaces.UserService;

import java.util.List;

@Controller
public class SubjectController {

    private final SubjectService subjectService;
    private final UserService userService;


    @Autowired
    public SubjectController(SubjectService subjectService, UserService userService) {
        this.subjectService = subjectService;
        this.userService = userService;
    }

    @GetMapping("/user/subjects{user-id}/add")
    public String getSaveSubjectsToProfilePage(Model model, @PathVariable("user-id") String userId){

        User user = userService.getUserById(userId);
        List<Subject> subjects = subjectService.findAllExceptUserAlreadyHave(user);
        SubjectCardDtoWrapper subjectCardDtoWrapper = subjectService.convertToSubjectDtoWrapper(subjects);
        model.addAttribute("subjectCardDtoWrapper", subjectCardDtoWrapper);

        return "profile-add-subjects";
    }

    @PostMapping("/user/profile/subjects/add")
    public String postSaveSubjectsToProfile(@RequestParam("userId") String userId, @ModelAttribute SubjectCardDtoWrapper subjectCardDtoWrapper, Authentication authentication){
        subjectService.saveSubjectsToAuthenticatedUser(authentication, subjectCardDtoWrapper);
        return "redirect:/user/subjects" + userId;
    }

    @GetMapping("/user/subjects{user-id}")
    public String getUserSubjects(@PathVariable("user-id") String userId, @ModelAttribute("model") ModelMap model){
        User user = userService.getUserById(userId);
        List<SubjectAssociation> list= user.getProfile().getSubjects();
        model.addAttribute("subjects", list);
        return "profile-subjects";
    }
}
