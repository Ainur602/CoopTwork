package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.itis.model.User;
import ru.itis.security.roles.Role;
import ru.itis.security.states.State;
import ru.itis.services.interfaces.AuthenticationService;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Controller
public class LoginController {

    private AuthenticationService authService;

    @Autowired
    public LoginController(AuthenticationService authenticationService){
        this.authService = authenticationService;
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, Authentication authentication) {
        if (authentication != null) {
            request.getSession().invalidate();
        }
        return "redirect:/";
    }

//    @GetMapping("/login")
//    public String root(ModelMap model, RedirectAttributes attributes, Authentication authentication, @RequestParam Optional<String> error){
//        if(authentication != null){
//            User user = authService.getUserByAuthentication(authentication);
//            if (user.getState().equals(State.NOT_CONFIRMED)) {
//                attributes.addFlashAttribute("error", "Check your email address to confirm registration "+ user.getEmail() );
//                return "redirect:/logout";
//            }
//            if (user.getRole().equals(Role.ADMIN))
//                return "redirect:/admin/users";
//        }
//
//        if (error.isPresent()){
//            attributes.addFlashAttribute("error", error.get());
//        }
//        return "redirect:/";
//    }


}
