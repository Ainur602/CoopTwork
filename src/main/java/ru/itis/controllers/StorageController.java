package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.itis.services.interfaces.FileService;

import javax.servlet.http.HttpServletResponse;

@Controller
public class StorageController {

  private final FileService fileService;

  @Autowired
  public StorageController(FileService fileService) {
    this.fileService = fileService;
  }

  @GetMapping("/files/{file-name:.+}")
    public void getFile(@PathVariable("file-name") String fileName,
                        HttpServletResponse response) {
        fileService.writeFileToResponse(fileName, response);
    }
}
