package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.forms.GlobalNotificationForm;
import ru.itis.forms.GroupForm;
import ru.itis.model.GrEmpAssociation;
import ru.itis.model.Notification;
import ru.itis.model.User;
import ru.itis.services.interfaces.AuthenticationService;
import ru.itis.services.interfaces.NotificationService;
import ru.itis.services.interfaces.UserService;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class NotificationController {

    private final UserService userService;
    private final NotificationService notificationService;

    @Autowired
    public NotificationController(UserService userService, NotificationService notificationService) {
        this.userService = userService;
        this.notificationService = notificationService;
    }

    /**
     * @return Authenticated User`s notifications. For example when he invited to the group
     */
    @GetMapping("/user/id{user-id}/notifications")
    @Transactional
    public String getNotifications(Model model, @PathVariable("user-id") String userId){
        User user = userService.getUserById(userId);

        List<Notification> invites = user.getProfile().getNotifications();
        notificationService.checkNotifications(invites);
        model.addAttribute("invites", invites);
        return "notifications";
    }


    @GetMapping("/admin/notification/create")
    public String GetPageForCreateGlobalNotification(){
        return "create-global-notification";
    }

    @PostMapping("/admin/notification/create")
    public String createGlobalNotification(@ModelAttribute GlobalNotificationForm notificationForm){
        notificationService.createGlobalNotification(notificationForm);
        return "redirect:/admin/dashboard/users";
    }
}
