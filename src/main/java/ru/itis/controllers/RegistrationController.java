package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.services.interfaces.RegistrationService;
import ru.itis.validators.UserRegistrationFormValidator;

import javax.validation.Valid;
import java.util.UUID;

@Controller
public class RegistrationController {

    private final RegistrationService registrationService;
    private final UserRegistrationFormValidator userRegistrationFormValidator;

    @Autowired
    public RegistrationController(RegistrationService registrationService, UserRegistrationFormValidator userRegistrationFormValidator) {
        this.registrationService = registrationService;
        this.userRegistrationFormValidator = userRegistrationFormValidator;
    }

    @InitBinder("userForm")
    public void initUserFormValidator(WebDataBinder binder){
        binder.addValidators(userRegistrationFormValidator);
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute("userForm")UserRegistrationForm userRegistrationForm,
                               BindingResult errors, RedirectAttributes attributes){
        if (errors.hasErrors()){
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/register";
        }
        registrationService.register(userRegistrationForm);
        attributes.addFlashAttribute("message", "Please check Your email address to complete registration");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String getSignUpPage() {
        return "registration-page";
    }

    @GetMapping("/confirm/{user-uuid}")
    public String confirmUser(@PathVariable("user-uuid") String userUuid, RedirectAttributes attributes) {
        String res = registrationService.confirm(UUID.fromString(userUuid));
        if (res.equals("confirmed")){
            attributes.addFlashAttribute("message", "Your account successfully confirmed");
            return "redirect:/";
        }
        else {
            attributes.addFlashAttribute("error", "Wrong link");
            return "redirect:/register";
        }
    }
}
