package ru.itis.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
public class WelcomePageController {

    @GetMapping("/")
    public String indexPage(){
        return "index";
    }

    @GetMapping("/login")
    public String indexErrorPage(ModelMap model, @RequestParam Optional<String> error){
        error.ifPresent(s -> model.addAttribute("error", s));
        return "index";
    }
}
