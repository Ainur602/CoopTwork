package ru.itis.interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import ru.itis.model.User;
import ru.itis.repository.UserRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {

    private final UserRepository userRepository;

    @Autowired
    public AuthInterceptor(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * In every page we need to get count_of_notifications and UserEntity
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        // if not authenticated user then
        if (!(authentication instanceof AnonymousAuthenticationToken)){
            // User
            String name  = authentication.getName();
            Optional<User> user = userRepository.findOneByLogin(name);
            request.setAttribute("auth_user", user.get());
            // Notifications
            Long notCount = user.get().getProfile().getNotifications().stream()
                .filter(o -> o.getIsChecked().equals(false))
                .count();
            request.setAttribute("notification_count", notCount);
        }
        super.postHandle(request, response, handler, modelAndView);
    }
}
