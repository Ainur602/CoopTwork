package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.model.Employer;

public interface EmployerRepository extends JpaRepository<Employer, Long> {
    Employer findOneByUser_Id(Long id);
    Employer findOneByUser_Login(String login);
    void deleteById(Long id);
}
