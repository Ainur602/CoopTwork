package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Employee findOneByUser_Id(Long id);
}
