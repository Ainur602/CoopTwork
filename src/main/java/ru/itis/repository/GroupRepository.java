package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.model.Group;

public interface GroupRepository extends JpaRepository<Group, Long> {
    Group findOneByName(String name);
}
