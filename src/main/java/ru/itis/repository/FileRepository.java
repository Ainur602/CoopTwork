package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.model.File;

public interface FileRepository extends JpaRepository<File, Long> {
  File findOneByStorageFileName(String storageFileName);

}
