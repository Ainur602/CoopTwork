package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itis.model.Subject;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long>{
    Subject findOneByName(String name);
    Subject findOneById(Long id);
}
