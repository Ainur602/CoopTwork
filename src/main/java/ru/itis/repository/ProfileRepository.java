package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itis.model.Profile;
@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {
    Profile findOneByName(String name);

}
