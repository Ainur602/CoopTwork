package ru.itis.security.providers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.itis.model.User;
import ru.itis.repository.UserRepository;
import ru.itis.security.states.State;

import java.util.Collection;
import java.util.Optional;

@Component
public class AuthProvider implements AuthenticationProvider {

    private final UserRepository userRepository;

    private final UserDetailsService userDetailsService;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    @Autowired
    public AuthProvider(UserRepository userRepository,@Qualifier("generalUserDetailsService") UserDetailsService userDetailsService) {
        this.userRepository = userRepository;
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();


        Optional<User> existedUser = this.userRepository.findOneByLogin(username);

        if(!existedUser.isPresent()){
            throw new BadCredentialsException("Wrong password or login");
        }

        existedUser.ifPresent(user -> {

            if(!passwordEncoder.matches(password, user.getPassword())){
                throw new BadCredentialsException("Wrong password or login");
            }
            if (!user.getState().equals(State.CONFIRMED)){
                throw new BadCredentialsException("User is not confirmed");
            }
        });

        UserDetails details = this.userDetailsService.loadUserByUsername(username);
        Collection<? extends GrantedAuthority> authorities = details.getAuthorities();
        return new UsernamePasswordAuthenticationToken(details, password, authorities);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(UsernamePasswordAuthenticationToken.class.getName());
    }
}
