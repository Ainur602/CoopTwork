package ru.itis.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.forms.CommentDto;
import ru.itis.model.Comment;
import ru.itis.model.Group;
import ru.itis.model.Profile;
import ru.itis.repository.CommentRepository;
import ru.itis.repository.GroupRepository;
import ru.itis.repository.ProfileRepository;
import ru.itis.repository.UserRepository;
import ru.itis.services.interfaces.CommentService;


import java.sql.Timestamp;
import java.util.Date;

@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final ProfileRepository profileRepository;
    private final GroupRepository groupRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository, ProfileRepository profileRepository, GroupRepository groupRepository) {
        this.commentRepository = commentRepository;
        this.profileRepository = profileRepository;
        this.groupRepository = groupRepository;
    }

    @Override
    @Transactional
    public void save(CommentDto commentDto) {

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Profile profile = profileRepository.getOne(commentDto.getUserId());
        Group group = groupRepository.getOne(commentDto.getGroupId());

        Comment comment = Comment.builder()
                .addedDate(timestamp)
                .author(profile)
                .group(group)
                .text(commentDto.getText())
                .build();
        commentRepository.save(comment);
    }
}