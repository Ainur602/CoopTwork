package ru.itis.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import ru.itis.model.User;
import ru.itis.repository.UserRepository;
import ru.itis.security.details.UserDetailsImpl;
import ru.itis.services.interfaces.AuthenticationService;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {


    @Override
    public User getUserByAuthentication(Authentication auth) {
        UserDetailsImpl currentUserDetails = (UserDetailsImpl) auth.getPrincipal();
        return currentUserDetails.getUser();
    }
}
