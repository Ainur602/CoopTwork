package ru.itis.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.forms.SubjectCardDto;
import ru.itis.forms.SubjectCardDtoWrapper;
import ru.itis.forms.SubjectForm;
import ru.itis.model.File;
import ru.itis.model.Subject;
import ru.itis.model.SubjectAssociation;
import ru.itis.model.User;
import ru.itis.repository.SubjectRepository;
import ru.itis.services.interfaces.AuthenticationService;
import ru.itis.services.interfaces.FileService;
import ru.itis.services.interfaces.SubjectService;
import ru.itis.utils.FileStorageUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubjectServiceImpl implements SubjectService {

    private final SubjectRepository subjectRepository;
    private final FileService fileService;
    private final FileStorageUtil fileStorageUtil;
    private final AuthenticationService authenticationService;
    private final EntityManager entityManagerl;

    @Autowired
    public SubjectServiceImpl(SubjectRepository subjectRepository, FileService fileService, FileStorageUtil fileStorageUtil, EntityManager entityManagerl, AuthenticationService authenticationService) {
        this.subjectRepository = subjectRepository;
        this.fileService = fileService;
        this.fileStorageUtil = fileStorageUtil;
        this.entityManagerl = entityManagerl;
        this.authenticationService = authenticationService;
    }

    @Override
    public List<Subject> findAll() {
        return subjectRepository.findAll();
    }

    @Override
    public Subject findOneById(String id) {
        Long subjectId = Long.valueOf(id);
        return subjectRepository.findOneById(subjectId);
    }

    @Override
    public String save(SubjectForm subjectForm) {

        File file = fileService.saveFile(subjectForm.getImage());

        Subject subject = Subject.builder()
                .name(subjectForm.getName())
                .description(subjectForm.getDescription())
                .image(file)
                .build();

        subjectRepository.save(subject);
        return  subject.getName();
    }

    @Override
    public String delete(Subject subject) {
        fileStorageUtil.deleteFromStorage(subject.getImage().getStorageFileName());
        subjectRepository.delete(subject);
        return subject.getName();
    }

    @Override
    public SubjectCardDtoWrapper convertToSubjectDtoWrapper(List<Subject> subjects) {


        List<SubjectCardDto> subjectCardDtoList =
                subjects.stream()
                        .map(o -> new SubjectCardDto(o.getId(),true, o.getName(), 0, o.getDescription(), o.getImage()))
                        .collect(Collectors.toList());

        return new SubjectCardDtoWrapper(subjectCardDtoList);
    }

    @Override
    @Transactional
    public void saveSubjectsToAuthenticatedUser(Authentication authentication, SubjectCardDtoWrapper subjectCardDtoWrapper) {
        User user = authenticationService.getUserByAuthentication(authentication);

        List<SubjectCardDto> SCList = subjectCardDtoWrapper.getCardDtoList();
        for (int i = 0; i <  SCList.size(); i++) {
            if (SCList.get(i).getIsChecked() != null && SCList.get(i).getIsChecked()){
                SubjectAssociation association = new SubjectAssociation();
                association.setProfile(user.getProfile());
                association.setRating(SCList.get(i).getRating());
                association.setSubject(subjectRepository.getOne(SCList.get(i).getSubjectId()));
                entityManagerl.merge(association);
            }
        }
    }


    @Override
    public List<Subject> findAllExceptUserAlreadyHave(User user) {

        List<Subject> subjects = subjectRepository.findAll();
        List<Subject> userSubjects = user.getProfile().getSubjects()
                .stream()
                .map(SubjectAssociation::getSubject)
                .collect(Collectors.toList());
        List<Subject> subjectsResult = new ArrayList<>();

        for (int i = 0; i < subjects.size() ; i++) {
            Subject subject = subjects.get(i);
            if (!userSubjects.contains(subject)){
                subjectsResult.add(subject);
            }
        }
        return subjectsResult;
    }


}
