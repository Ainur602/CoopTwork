package ru.itis.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.model.Employee;
import ru.itis.model.GrEmpAssociation;
import ru.itis.model.GrEmpAssociationPK;
import ru.itis.model.Group;
import ru.itis.services.interfaces.EmpGroupAssociationService;
import ru.itis.services.interfaces.NotificationService;

import javax.persistence.EntityManager;

@Service
public class EmpGroupAssociationServiceImpl implements EmpGroupAssociationService {

    private final EntityManager entityManager;
    private final NotificationService notificationService;

    @Autowired
    public EmpGroupAssociationServiceImpl(EntityManager entityManager, NotificationService notificationService) {
        this.entityManager = entityManager;
        this.notificationService = notificationService;
    }

    @Override
    @Transactional
    public void joinToGroup(Long groupId, Long employeeId) {
        GrEmpAssociation association = entityManager.find(GrEmpAssociation.class, new GrEmpAssociationPK(Long.valueOf(groupId), Long.valueOf(employeeId)));
        association.setIsAccepted(true);
        entityManager.persist(association);
    }

    @Override
    @Transactional
    public void inviteEmployeeToGroup(Group group, Employee employee) {
        GrEmpAssociation grEmpAssociation = new GrEmpAssociation();

        notificationService.inviteEmployeeToGroup(employee.getId(), group);
        grEmpAssociation.setGroup(group);
        grEmpAssociation.setEmployee(employee);
        entityManager.persist(grEmpAssociation);
    }

    @Override
    @Transactional
    public boolean deleteUserFromGroup(Group group, Employee employee) {
        GrEmpAssociation association = null;
        for (int i = 0; i < group.getEmployees().size(); i++) {
            if (group.getEmployees().get(i).getEmployee().equals(employee)){
                association = group.getEmployees().get(i);
                break;
            }
        }
        group.getEmployees().remove(association);
        employee.getGroups().remove(association);
        entityManager.remove(association);

        // If Employer true
        return group.getEmployer().getId().equals(employee.getId());
    }
}
