package ru.itis.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.forms.GroupForm;
import ru.itis.model.*;
import ru.itis.repository.GroupRepository;
import ru.itis.repository.UserRepository;
import ru.itis.services.interfaces.EmpGroupAssociationService;
import ru.itis.services.interfaces.FileService;
import ru.itis.services.interfaces.GroupService;
import ru.itis.utils.FileStorageUtil;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupServiceImpl implements GroupService {
    private final GroupRepository groupRepository;
    private final FileService fileService;
    private final FileStorageUtil fileStorageUtil;
    private final UserRepository userRepository;


    @Autowired
    public GroupServiceImpl(GroupRepository groupRepository, FileService fileService, UserRepository userRepository, FileStorageUtil fileStorageUtil) {
        this.groupRepository = groupRepository;
        this.fileService = fileService;
        this.userRepository = userRepository;
        this.fileStorageUtil = fileStorageUtil;

    }

    @Override
    public List<Group> findAll() {
        return groupRepository.findAll();
    }

    @Override
    public Long save(User user, GroupForm groupForm) {

        Group group = Group.builder()
                .name(groupForm.getName())
                .about(groupForm.getAbout())
                .description(groupForm.getDescription())
                .employer(user.getEmployer())
                .build();
        if (!groupForm.getImage().getOriginalFilename().equals("")) {
            File file = fileService.saveFile(groupForm.getImage());
            group.setImage(file);
        }
        groupRepository.save(group);
        return group.getId();
    }

    @Override
    public Group getGroupById(String groupId) {
        return groupRepository.getOne(Long.valueOf(groupId));
    }

    @Override
    public List<Group> getAllCreatedGroupsByUser(String userId) {
        User user = userRepository.getOne(Long.valueOf(userId));
        return user.getEmployer().getGroups();
    }

    @Override
    public List<Employee> getAcceptedGroupMembers(Group group) {
        return group.getEmployees()
                .stream()
                .filter(o -> o.getIsAccepted().equals(true))
                .map(GrEmpAssociation::getEmployee)
                .collect(Collectors.toList());
    }

    @Override
    public List<Group> getAllGroupWhereUserIsEmployee(String userId) {
        User user = userRepository.getOne(Long.valueOf(userId));
        return user.getEmployee().getGroups().stream()
                .map(GrEmpAssociation::getGroup)
                .collect(Collectors.toList());
    }

    @Override
    public Boolean hasUserAccessToGroup(User user, Group group) {
        boolean flag1 = group.getEmployer().getId().equals(user.getId());
        boolean flag2 = group.getEmployees().stream()
                .anyMatch(o -> o.getEmployee().getId().equals(user.getId()));
        return flag1 || flag2;
    }

    @Override
    @Transactional
    public void update(Group group, GroupForm groupForm) {
        if (!groupForm.getName().equals(null)){
            group.setName(groupForm.getName());
        }
        if (!groupForm.getAbout().equals(null)){
            group.setAbout(groupForm.getAbout());
        }
        if (!groupForm.getDescription().equals(null)){
            group.setDescription(groupForm.getDescription());
        }
        if (!groupForm.getImage().getOriginalFilename().equals("")){
            fileStorageUtil.deleteFromStorage(group.getImage().getStorageFileName());
            File file = fileService.saveFile(groupForm.getImage());
            group.setImage(file);
        }
        groupRepository.save(group);
    }

    @Override
    @Transactional
    public void delete(Group group) {
        if (group.getImage() != null) {
            fileStorageUtil.deleteFromStorage(group.getImage().getStorageFileName());
        }
        List<GrEmpAssociation> grEmpAssociationsList = group.getEmployees();
        for (int i = 0; i < grEmpAssociationsList.size(); i++) {
            GrEmpAssociation association = grEmpAssociationsList.get(0);
            association.getEmployee().getGroups().remove(association);
        }
        groupRepository.delete(group);
    }
}