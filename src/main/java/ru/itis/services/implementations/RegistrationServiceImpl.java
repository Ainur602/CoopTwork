package ru.itis.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.model.Employee;
import ru.itis.model.Employer;
import ru.itis.model.Profile;
import ru.itis.model.User;
import ru.itis.repository.UserRepository;
import ru.itis.security.roles.Role;
import ru.itis.security.states.State;
import ru.itis.services.interfaces.EmailService;
import ru.itis.services.interfaces.RegistrationService;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    private final UserRepository userRepository;
    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    private final EmailService emailService;
    private ExecutorService executorService = Executors.newCachedThreadPool();


    @Value("${url.confirm.path}")
    private String urlConfirmPath;

    @Autowired
    public RegistrationServiceImpl(UserRepository userRepository, EmailService emailService) {
        this.userRepository = userRepository;
        this.emailService = emailService;
    }

    @Override
    public void register(UserRegistrationForm userRegistrationForm) {
        User newUser = User.builder()
                .email(userRegistrationForm.getEmail())
                .login(userRegistrationForm.getLogin())
                .password(passwordEncoder.encode(userRegistrationForm.getPass()))
                .state(State.NOT_CONFIRMED)
                .role(Role.USER)
                .uuid(UUID.randomUUID())
                .build();

        Employee employee = new Employee();
        employee.setUser(newUser);

        Employer employer = new Employer();
        employer.setUser(newUser);

        Profile profile = Profile.builder()
                .name(userRegistrationForm.getName())
                .surname(userRegistrationForm.getSurname())
                .user(newUser)
                .build();

        newUser.setProfile(profile);
        newUser.setEmployee(employee);
        newUser.setEmployer(employer);

        executorService.submit(() -> {
            emailService.sendMail("If You were registered in Website Deb</>World, to complete registration follow this link <a href=\"" + urlConfirmPath
                            + newUser.getUuid() + "\"> <h4>Confirm account</h4></a1>",

                    "Подтверждение пользователя " + newUser.getLogin(),
                    newUser.getEmail());
        });

        userRepository.save(newUser);
    }

    @Override
    public String confirm(UUID uuid) {
        Optional<User> userO = userRepository.findOneByUuid(uuid);
        if (!userO.isPresent()){
            return "not_confitmed";
        }
        else {
            User user = userO.get();
            user.setUuid(null);
            user.setState(State.CONFIRMED);
            userRepository.save(user);
            return "confirmed";
        }
    }
}
