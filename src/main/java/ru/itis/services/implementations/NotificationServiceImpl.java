package ru.itis.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.forms.GlobalNotificationForm;
import ru.itis.model.*;
import ru.itis.repository.NotificationRepository;
import ru.itis.repository.ProfileRepository;
import ru.itis.repository.UserRepository;
import ru.itis.services.interfaces.FileService;
import ru.itis.services.interfaces.NotificationService;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class NotificationServiceImpl implements NotificationService {

    private final NotificationRepository notificationRepository;
    private final ProfileRepository profileRepository;
    private final FileService fileService;
    private final UserRepository userRepository;


    @Autowired
    public NotificationServiceImpl(NotificationRepository notificationRepository, ProfileRepository profileRepository, FileService fileService, UserRepository userRepository) {
        this.notificationRepository = notificationRepository;
        this.profileRepository = profileRepository;
        this.fileService = fileService;
        this.userRepository = userRepository;
    }

    @Override
    public void inviteEmployeeToGroup(Long profileId, Group group) {
        Profile employee = profileRepository.findById(profileId).get();
        Profile creator = profileRepository.findById(group.getEmployer().getId()).get();
        LocalDateTime time = LocalDateTime.now();

        Notification notification = Notification.builder()
                .isInvite(true)
                .isChecked(false)
                .groupId(group.getId())
                .profile(employee)
                .image(group.getImage())
                .addedDate(time)
                .text("<a href=\"/user/id"+ creator.getId() + "\">\n" + creator.getName() + " " + creator.getSurname() + "\n" +
                        "                            </a>\n" +
                        "                            invited you to <a href=\"/groups/group" + group.getId() + "\">\n" +
                        "                        " + group.getName() + "\n" +
                        "                        </a> group</p>")
                .build();
        notificationRepository.save(notification);
    }

    @Override
    @Transactional
    public void checkNotifications(List<Notification> uncheckedNotifications) {
        for (int i = 0; i < uncheckedNotifications.size() ; i++) {
            if (!uncheckedNotifications.get(i).getIsChecked())
                uncheckedNotifications.get(i).setIsChecked(true);
                notificationRepository.save(uncheckedNotifications.get(i));
        }
    }

    @Override
    public void notifyEmployerAboutRejection(Group group, Employee employee) {
        Profile profile = profileRepository.findById(employee.getId()).get();
        LocalDateTime time = LocalDateTime.now();

        Notification notification = Notification.builder()
                .isInvite(false)
                .isChecked(false)
                .groupId(group.getId())
                .profile(profileRepository.findById(group.getEmployer().getId()).get())
                .image(group.getImage())
                .addedDate(time)
                .text("<a href=\"/user/id"+ profile.getId() + "\">\n" + profile.getName() + " " + profile.getSurname() + "\n" +
                        "                            </a>\n" +
                        "                            rejected your invite to <a href=\"/groups/group" + group.getId() + "\">\n" +
                        "                        " + group.getName() + "\n" +
                        "                        </a> group</p>")
                .build();
        notificationRepository.save(notification);
    }

    @Override
    public void deleteNotification(String notificationId) {
        notificationRepository.deleteById(Long.valueOf(notificationId));
    }

    @Override
    public void createGlobalNotification(GlobalNotificationForm notificationForm) {
        boolean imageNotNull = !notificationForm.getImage().getOriginalFilename().equals("");
        File file = fileService.saveFile(notificationForm.getImage());
        LocalDateTime time = LocalDateTime.now();

        userRepository.findAll()
                .forEach(o -> {
                    Notification notification = Notification.builder()
                            .isInvite(false)
                            .isChecked(false)
                            .profile(o.getProfile())
                            .addedDate(time)
                            .text(notificationForm.getMessage())
                            .build();
                    if (imageNotNull){
                        notification.setImage(file);
                    }
                    notificationRepository.save(notification);
                });
    }
}
