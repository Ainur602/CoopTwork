package ru.itis.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.model.Profile;
import ru.itis.model.Subject;
import ru.itis.model.SubjectAssociation;
import ru.itis.services.interfaces.ProfileService;

import javax.persistence.EntityManager;

@Service
public class ProfileServiceImpl implements ProfileService{

    private final EntityManager em;

    @Autowired
    public ProfileServiceImpl(EntityManager em) {
        this.em = em;
    }

    public void deleteSubjectFromProfile(Profile profile, Subject subj) {
        for (int i = 0; i < profile.getSubjects().size(); i++) {
            SubjectAssociation association = profile.getSubjects().get(i);
            if (association.getSubject().equals(subj)) {
                association.getSubject().getProfiles().remove(association); // delete association in profile
                association.getProfile().getSubjects().remove(association); // delete association in subject
                association.setProfile(null);
                association.setSubject(null);
                em.remove(association);
            }
        }
    }

    public void associateSubject(Profile profile, Subject subject, int rating) {
        SubjectAssociation association3 = new SubjectAssociation();
        association3.setProfile(profile);
        association3.setSubject(subject);
        association3.setRating(rating);
        em.persist(association3);
    }

    public void associateSubject(Profile profile, Subject subject) {
        SubjectAssociation association3 = new SubjectAssociation();
        association3.setProfile(profile);
        association3.setSubject(subject);
        em.persist(association3);
    }
}
