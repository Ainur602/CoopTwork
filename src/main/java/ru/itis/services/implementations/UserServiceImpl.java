package ru.itis.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.forms.EditProfileForm;
import ru.itis.model.*;
import ru.itis.repository.ProfileRepository;
import ru.itis.repository.UserRepository;
import ru.itis.services.interfaces.AuthenticationService;
import ru.itis.services.interfaces.FileService;
import ru.itis.services.interfaces.UserService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

  private final ProfileRepository profileRepository;
  private final AuthenticationService authService;
  private final UserRepository userRepository;
  private final FileService fileService;

  @Autowired
  public UserServiceImpl(ProfileRepository profileRepository, AuthenticationService authService, UserRepository userRepository, FileService fileService) {
    this.profileRepository = profileRepository;
    this.authService = authService;
    this.userRepository = userRepository;
    this.fileService = fileService;
  }

  @Override
  public void updateProfile(Authentication authentication, EditProfileForm profileForm) {
    Profile userProfile = authService.getUserByAuthentication(authentication).getProfile();
    userProfile.setName(profileForm.getName());
    userProfile.setSurname(profileForm.getSurname());
    userProfile.setBirthday(profileForm.getBirthday());
    userProfile.setAbout(profileForm.getAbout());
    fileService.saveProfileAvatar(profileForm.getAvatar(), userProfile);
    profileRepository.save(userProfile);
  }

  @Override
  public List<User> findAll() {
    return userRepository.findAll();
  }

  @Override
  public User getUserById(String userId) {
    return userRepository.getOne(Long.valueOf(userId));
  }

  @Override
  public List<User> findByLoginContaining(String login) {
    return userRepository.findByLoginContaining(login);
  }

  @Override
  public List<User> findByLoginContaining(Group group, String login) {
    List<User> employees = group.getEmployees().stream()
            .map(o  -> o.getEmployee().getUser())
            .collect(Collectors.toList());

    List<User> userList = userRepository.findByLoginContaining(login);

    return userList.stream()
            .filter( o -> !employees.contains(o))
            .collect(Collectors.toList());
  }
}
