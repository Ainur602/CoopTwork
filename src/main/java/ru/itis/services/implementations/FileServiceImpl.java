package ru.itis.services.implementations;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.model.File;
import ru.itis.model.Profile;
import ru.itis.repository.FileRepository;
import ru.itis.repository.ProfileRepository;
import ru.itis.services.interfaces.FileService;
import ru.itis.utils.FileStorageUtil;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.InputStream;

@Service
public class FileServiceImpl implements FileService {

  private final FileRepository fileRepository;
  private final FileStorageUtil fileStorageUtil;
  private final ProfileRepository profileRepository;

  @Autowired
  public FileServiceImpl(FileRepository fileRepository, FileStorageUtil fileStorageUtil, ProfileRepository profileRepository) {
    this.fileRepository = fileRepository;
    this.fileStorageUtil = fileStorageUtil;
    this.profileRepository = profileRepository;
  }

  @Override
  public String saveProfileAvatar(MultipartFile multipartFile, Profile profile) {
    if (!multipartFile.getOriginalFilename().equals("")) {
      // конвертируем из Multipart в понятный для нас объект БД
      File avatar = fileStorageUtil.convertFromMultipart(multipartFile);
      // сохраняем информацию о файле
      fileRepository.save(avatar);
      if (profile.getAvatar() != null) {
        fileRepository.delete(profile.getAvatar());
      }
      profile.setAvatar(avatar);
      profileRepository.save(profile);
      // переносим файл на наш диск
      fileStorageUtil.copyToStorage(multipartFile, avatar.getStorageFileName());
      // возвращаем имя файла - новое
      return avatar.getStorageFileName();
    }else
      return profile.getAvatar().getStorageFileName();
  }

  @Override
  public File saveFile(MultipartFile multipartFile) {
    File file = fileStorageUtil.convertFromMultipart(multipartFile);
    fileStorageUtil.copyToStorage(multipartFile, file.getStorageFileName());
    return file;
  }

  @SneakyThrows
  @Override
  public void writeFileToResponse(String fileName, HttpServletResponse response) {
    File file = fileRepository.findOneByStorageFileName(fileName);
    response.setContentType(file.getType());
    // получили инпут стрим файла на диске
    InputStream inputStream = new FileInputStream(new java.io.File(fileStorageUtil.getStoragePath() + "//" + file.getStorageFileName()));
    // скопировали файл в ответ
    org.apache.commons.io.IOUtils.copy(inputStream, response.getOutputStream());

    inputStream.close();
    // пробрасываем буфер
    response.flushBuffer();
  }
}
