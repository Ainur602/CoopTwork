package ru.itis.services.interfaces;

import org.springframework.security.core.Authentication;
import ru.itis.forms.SubjectCardDtoWrapper;
import ru.itis.forms.SubjectForm;
import ru.itis.model.Subject;
import ru.itis.model.SubjectAssociation;
import ru.itis.model.User;

import java.util.List;

public interface SubjectService {
    List<Subject> findAll();

    Subject findOneById(String id);

    String save(SubjectForm subjectForm);

    String delete(Subject subject);

    SubjectCardDtoWrapper convertToSubjectDtoWrapper(List<Subject> subjects);

    void saveSubjectsToAuthenticatedUser(Authentication authentication, SubjectCardDtoWrapper subjectCardDtoWrapper);

    List<Subject> findAllExceptUserAlreadyHave(User user);
}
