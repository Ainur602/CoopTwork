package ru.itis.services.interfaces;

import ru.itis.forms.GroupForm;
import ru.itis.model.Employee;
import ru.itis.model.Group;
import ru.itis.model.User;

import java.util.List;

public interface GroupService {
    List<Group> findAll();

    Long save(User user, GroupForm groupForm);

    Group getGroupById(String groupId);


    List<Group> getAllCreatedGroupsByUser(String userId);

    /*
    Return accepted to the Group Employees, without Invited
     */
    List<Employee> getAcceptedGroupMembers(Group group);

    List<Group> getAllGroupWhereUserIsEmployee(String userId);

    Boolean hasUserAccessToGroup(User user, Group group);

    void update(Group group, GroupForm groupForm);

    void delete(Group group);
}
