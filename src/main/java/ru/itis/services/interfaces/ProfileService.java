package ru.itis.services.interfaces;

import ru.itis.model.Profile;
import ru.itis.model.Subject;

public interface ProfileService {

    void deleteSubjectFromProfile(Profile profile, Subject subj);

    void associateSubject(Profile profile, Subject subject, int rating);
}
