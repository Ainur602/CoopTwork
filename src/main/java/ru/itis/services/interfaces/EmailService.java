package ru.itis.services.interfaces;

public interface EmailService {
    void sendMail(String text, String subject, String email);
}
