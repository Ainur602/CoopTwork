package ru.itis.services.interfaces;

import org.springframework.security.core.Authentication;
import ru.itis.forms.EditProfileForm;
import ru.itis.model.Group;
import ru.itis.model.Profile;
import ru.itis.model.User;

import javax.validation.Valid;
import java.util.List;

public interface UserService {
    void updateProfile(Authentication authentication, @Valid EditProfileForm editProfileForm);
    List<User> findAll();

    User getUserById(String userId);

    List<User> findByLoginContaining(String login);
    List<User> findByLoginContaining(Group group, String login);
}
