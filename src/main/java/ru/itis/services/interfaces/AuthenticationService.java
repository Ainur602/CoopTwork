package ru.itis.services.interfaces;

import org.springframework.security.core.Authentication;
import ru.itis.model.User;

public interface AuthenticationService {
    User getUserByAuthentication(Authentication auth);
}
