package ru.itis.services.interfaces;

import ru.itis.forms.UserRegistrationForm;

import java.util.UUID;

public interface RegistrationService {
    void register(UserRegistrationForm userRegistrationForm);
    String confirm(UUID uuid);
}
