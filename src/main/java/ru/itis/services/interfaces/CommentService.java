package ru.itis.services.interfaces;

import ru.itis.forms.CommentDto;

public interface CommentService {
    void save(CommentDto commentDto);
}
