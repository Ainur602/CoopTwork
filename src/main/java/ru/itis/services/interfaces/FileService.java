package ru.itis.services.interfaces;

import org.springframework.web.multipart.MultipartFile;
import ru.itis.model.File;
import ru.itis.model.Profile;

import javax.servlet.http.HttpServletResponse;

public interface FileService {
  String saveProfileAvatar(MultipartFile avatar, Profile profile);
  File saveFile(MultipartFile multipartFile);
  void writeFileToResponse(String fileName, HttpServletResponse response);
}
