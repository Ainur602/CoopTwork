package ru.itis.services.interfaces;

import ru.itis.forms.GlobalNotificationForm;
import ru.itis.model.Employee;
import ru.itis.model.Group;
import ru.itis.model.Notification;

import java.util.List;

public interface NotificationService {
    void  inviteEmployeeToGroup(Long profileId, Group group);

    void checkNotifications(List<Notification> uncheckedNotifications);

    void notifyEmployerAboutRejection(Group group, Employee employee);

    void deleteNotification(String notificationId);

    void createGlobalNotification(GlobalNotificationForm notificationForm);
}
