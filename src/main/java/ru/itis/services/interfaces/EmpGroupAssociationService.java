package ru.itis.services.interfaces;

import ru.itis.model.Employee;
import ru.itis.model.GrEmpAssociation;
import ru.itis.model.Group;

public interface EmpGroupAssociationService {
    void joinToGroup(Long groupId, Long employeeId);

    void inviteEmployeeToGroup(Group group, Employee employee);

    boolean deleteUserFromGroup(Group group, Employee employee);

}
