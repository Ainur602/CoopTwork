<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${group.name} Team</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
<#include "include/header.ftl">

<div class="my-body index-body">
    <div class="container users-team-body">
        <h2 style="color: #333333" class="article-title"><a href="/groups/group${group.id}">${group.name}</a> Team</h2>
        <div style="margin-top: 2em" class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="button" class="btn btn-success btn-filter" data-target="accepted-true">Members
                            </button>
                            <button type="button" class="btn btn-warning btn-filter" data-target=accepted-false>Invited
                            </button>
                            <button type="button" class="btn btn-default btn-filter" data-target="all">All</button>
                        </div>
                    </div>
                    <div >
                        <table class="table table-filter">
                            <tbody>
                            <tr data-status="accepted-true}">
                                <td>
                                    <div class="ckbox">
                                        <input type="checkbox" id="checkbox3">
                                        <label for="checkbox3"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="media">
                                        <a href="/user/id${group.employer.id}" class="pull-left">
                                        <#if (group.employer.user.profile.avatar.url)??>
                                            <img src="${group.employer.user.profile.avatar.url}"
                                                 class="media-photo">
                                        <#else >
                                            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg"
                                                 class="media-photo">
                                        </#if>
                                        </a>
                                        <div class="media-body">
                                            <h4 class="title">
                                            ${group.employer.user.profile.name} ${group.employer.user.profile.surname}(${group.employer.user.login})

                                                <span class="pull-right pagado">(Admin)</span>
                                            </h4>
                                            <p class="summary">
                                            <#if (group.employer.user.profile.about)??>
                                            ${group.employer.user.profile.about}
                                            <#else >
                                                Unknown
                                            </#if>
                                            </p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <#list group.employees as member>

                            <tr data-status="accepted-${member.isAccepted?c}">
                                <td>
                                    <div class="ckbox">
                                        <input type="checkbox" id="checkbox3">
                                        <label for="checkbox3"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="media">
                                        <a href="/user/id${member.employee.user.id}" class="pull-left">
                                            <#if (member.employee.user.profile.avatar.url)??>
                                                <img src="${member.employee.user.profile.avatar.url}"
                                                     class="media-photo">
                                            <#else >
                                                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg"
                                                     class="media-photo">
                                            </#if>
                                        </a>
                                        <div class="media-body">
                                            <#if member.group.employer.id == auth_user.id>
                                                <span class="media-meta pull-right">
                                                    <form action="/groups/group${member.group.id}/members/remove" method="post">
                                                        <input type="hidden" name="employeeId" value="${member.employee.id}">
                                                        <button class="btn-remove" type="submit">
                                                            <i class="fa fa-remove"></i>
                                                        </button>
                                                    </form>
                                                </span>
                                            <#else >
                                            </#if>
                                            <h4 class="title">
                                                ${member.employee.user.profile.name} ${member.employee.user.profile.surname}(${member.employee.user.login})

                                                <#if member.isAccepted>
                                                    <span class="pull-right pagado">(Member)</span>
                                                <#else >
                                                    <span class="pull-right pendiente">(Invited)</span>
                                                </#if>
                                            </h4>
                                            <p class="summary">
                                                <#if (member.employee.user.profile.about)??>
                                                    ${member.employee.user.profile.about}
                                                <#else >
                                                    Unknown
                                                </#if>
                                            </p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </#list>
                            </tbody>
                        </table>
                        <div class="search-container">
                            <form action="/groups/group${group.id}/members/search" method="get">
                                <input type="text" placeholder="Search.." name="login">
                                <button style="height: 33px" class="btn-blue" type="submit">Invite</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<#include "include/footer.ftl">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/main.js"></script>
</body>
</html>