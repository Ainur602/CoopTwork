<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Notification</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
<#include "include/header.ftl">
<div class="my-body">
    <div class="container">
        <!-- Form Area -->
        <h2 class="article-title">Create notification form</h2>
        <hr>
        <!-- Form -->
        <form method="post" action="/admin/notification/create" enctype="multipart/form-data">
            <!-- Left Inputs -->
            <div class="col-xs-6" data-wow-delay=".5s">
                <!-- Message -->
                <textarea name="message" id="message" class="form textarea"
                          placeholder="Message"></textarea>
            </div><!-- End Left Inputs -->
            <!-- Right Inputs -->
            <div  style="margin-bottom: 1.5em" class="col-xs-6" data-wow-delay=".5s">
                <div class="create-img-zone col-md-4" id="drop-zone">
                    <div>Just drag and drop files here
                    </div>
                    <input type="file" name="image">
                </div>
            </div><!-- End Right Inputs -->
            <!-- Bottom Submit -->
            <div class="col-xs-12">
                <!-- Send Button -->
                <button type="submit" id="submit" name="submit" class="form-btn">Create</button>
            </div><!-- End Bottom Submit -->
        </form>
        <!-- End Contact Form Area -->
    </div>
</div>
<#include "include/footer.ftl">
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>