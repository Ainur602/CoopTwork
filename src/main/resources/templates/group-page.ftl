<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${group.name}</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
<#include "include/header.ftl">
<div class="my-body">
    <div class="container group-body">
        <h2 class="article-title">${group.name}</h2>
        <p class="author">
            Created by <a href="/user/id${group.employer.id}">${group.employer.user.login}</a>
        </p>
        <hr>
        <!--About-->
    <#if (group.image.url)??>
        <img src="${group.image.url}">
    <#else >
        <img src="/img/work.jpg">
    </#if>
        <div class="group-about">
            About:
        <#if (group.about)??>
        ${group.about}
        <#else >
        ${group.about}
        </#if>
        </div>
        <!--description-->
        <div class="group-article-description">
            Description:
        <#if (group.description)??>
        ${group.description}
        <#else >
        ${group.description}
        </#if>
        </div>
        <!--People-->
        <div class="members">
            <a href="/groups/group${group.id}/members">
                <span class="glyphicon glyphicon-user"></span>
                <div>Members : ${membersSize}</div>
            </a>
        </div>
        <div class="comment-title" data-toggle="collapse" href="#comments">
            Comments <i class="fa fa-arrow-down"></i>
        </div>

        <section class="comments collapse" id="comments">
        <#list group.comments as comment>
            <article class="comment">
                <a class="comment-img" href="#non">
                         <#if (comment.author.avatar.url)??>
                    <img src="${comment.author.avatar.url}" width="50" height="50">
                         <#else >
                             <img src="/img/you-rock.jpg" width="50" height="50">
                         </#if>
                </a>
                <div class="comment-body">
                    <div class="text">
                        <p>${comment.text}</p>
                    </div>
                    <p class="attribution">by <a
                            href="${comment.author.id}">${comment.author.name} ${comment.author.surname}</a>
                        at ${comment.addedDate}</p>
                </div>
            </article>
        <#else >
            <div style="margin-left: 5em">
                No one left comments yet
            </div>

        </#list>
            <div class="comment-send-block">
                <form action="/comments/add" method="post">
                    <div class="comment-body">
                        <textarea id="message" name="text" placeholder="Your Message" rows="5"></textarea>
                    </div>
                    <input type="hidden" name="userId" value="${auth_user.id}">
                    <input type="hidden" name="groupId" value="${group.id}">
                    <div class="comment-body">
                        <button type="submit" class="btn-blue pull-left"><span class="glyphicon glyphicon-send"></span>
                            Send
                        </button>
                    </div>
                </form>
            </div>
        </section>
        <div class="col-xs-12 group-btns">
        <#if auth_user.id == group.employer.id>
            <a href="/groups/group${group.id}/edit">
                <button type="button" class="group-btn-edit">Edit</button>
            </a>
            <a href="#" data-href="${group.id}" data-toggle="modal" data-target="#confirm-delete">
                    <button type="button" class="group-btn-del">Delete</button>
            </a>
        <#else >
            <a href="">
                <form action="/groups/group${group.id}/members/remove" method="post">
                    <input type="hidden" name="employeeId" value="${auth_user.id}">
                    <button type="submit" class="group-btn-del">Leave</button>
                </form>
            </a>
        </#if>
        </div>

    </div>
</div>
<#--Modal delete confirmation window-->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title" id="myModalLabel">Confirm Delete</h2>
            </div>

            <div class="modal-body">
                <p>You are about to delete one track, this procedure is irreversible.</p>
                <p>Do you want to proceed?</p>
            <#--<p class="debug-url"></p>-->
            </div>
            <form action="/groups/delete" method="post">
                <div class="modal-footer">
                    <input type="hidden" id="delete-group-id" name="groupId" value="0">
                    <button type="button" style="float: right" class="group-btn-edit" data-dismiss="modal">Cancel</button>
                    <button type="submit" style="float: right; margin-right: 1em" class="group-btn-del  btn-ok">Delete</button>

                <#--<a class="btn btn-danger btn-ok">Delete</a>-->
                </div>
            </form>
        </div>
    </div>
</div>
<#--Modal window end-->
<#include "include/footer.ftl">
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        document.getElementById("delete-group-id").value = $(this).find('.btn-ok').attr('href');
        $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
    });
</script>
</body>
</html>