<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Registartion</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>

<#include "include/header.ftl">


<#if error??>
<div style="margin: 0; text-align: center" class="alert alert-danger" role="alert">${error}</div>
</#if>
<section class="pb_cover_v3 overflow-hidden cover-bg-indigo cover-bg-opacity text-left pb_gradient_v1 pb_slant-light" id="section-home">
    <div class="container">
        <div class="register-body row align-items-center justify-content-center">
            <div class="col-md-6">
                <h2 class="heading mb-3">Let`s code faster</h2>
                <div class="sub-heading">
                    <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum leo mi, faucibus id sem efficitur, blandit bibendum elit. Aliquam vitae augue orci. Donec lacinia metus eget orci lobortis posuere vitae sed leo.</p>
                    <p class="mb-5"><a class="btn btn-success btn-lg pb_btn-pill smoothscroll" href="/"><span class="pb_font-14 pb_letter-spacing-1">Dev<span class="fa fa-code" aria-hidden="true"></span>World</span></a></p>
                </div>
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-5 relative align-self-center">
                <form action="/register" method="post" class="bg-white rounded pb_form_v1">
                    <h2 class="mb-4 mt-0 text-center">Sign Up for Free</h2>
                    <div class="form-group">
                        <input name="name" type="text" class="form-control pb_height-50 reverse" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <input name="surname" type="text" class="form-control pb_height-50 reverse" placeholder="Surname">
                    </div>
                    <div class="form-group">
                        <input name="login" type="text" class="form-control pb_height-50 reverse" placeholder="Login">
                    </div>
                    <div class="form-group">
                        <input name="email" type="text" class="form-control pb_height-50 reverse" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input name="pass" type="password" class="form-control pb_height-50 reverse" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <input name="passCon" type="password" class="form-control pb_height-50 reverse" placeholder="Password repeat">
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-lg btn-block pb_btn-pill ora-btn btn-shadow-orange" value="Register">
                    </div>
                </form>

            </div>
        </div>
    </div>
</section>
<!-- END section -->


<footer class="pb_footer bg-light" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <p>2018</p>
                <p>© Kazan Federal University </p>
            </div>
        </div>
    </div>
</footer>
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>