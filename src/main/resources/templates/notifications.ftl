<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Index</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
<#include "include/header.ftl">

<div class="index-body">
    <div class="container">

        <ul class="notifications">
            <h2> Notifications</h2>

            <#list invites?sort_by("addedDate")?reverse as invite>
                <li>
                <div class="media">
                    <#if invite.isInvite>

                        <div class="pull-right">
                            <form action="/groups/group${invite.groupId}/join" method="post">
                                <input type="hidden" name="employeeId" value="${auth_user.id}">
                                <input type="hidden" name="inviteId" value="${invite.id}">
                                <button type="submit" class="btn-blue">Accept</button>
                            </form>
                            <form action="/groups/group${invite.groupId}/reject" method="post">
                                <input type="hidden" name="employeeId" value="${auth_user.id}">
                                <input type="hidden" name="inviteId" value="${invite.id}">
                                <button type="submit" class="btn-decline">Decline</button>
                            </form>
                        </div>

                        <#if (invite.image)??>
                        <div class="media-left">
                            <div class="media-object">
                                <#if (invite.image.url)??>
                                    <img src="${invite.image.url}" class="img-circle">
                                <#else >
                                    <img src="/img/employer.jpg" class="img-circle">
                                </#if>
                            </div>
                        </div>
                        </#if >
                        <div class="media-body">
                            <p class="notification-title">
                                ${invite.text}
                            <#--<a href="/user/id${invite.group.employer.id}">-->
                                <#--${invite.group.employer.user.profile.name} ${invite.group.employer.user.profile.surname}-->
                            <#--</a>-->
                            <#--invited you to <a href="/groups/group${invite.group.id}">-->
                        <#--${invite.group.name}-->
                        <#--</a> group</p>-->
                            <p class="notification-desc">You were invited to the group at</p>
                            <div class="notification-meta">
                                <small class="timestamp">
                                    Time ${invite.addedDate.getHour()}:${invite.addedDate.getMinute()}

                                    day
                                    ${invite.addedDate.getDayOfMonth()} ${invite.addedDate.getMonth()} ${invite.addedDate.getYear()}
                                </small>
                            </div>
                        </div>

                        <#else >  <#--if it is not invite-->


                                <div class="media-left">
                                    <div class="media-object">
                                        <#if (invite.image.url)??>
                                            <img src="${invite.image.url}" class="img-circle">
                                        <#else >
                                            <img src="/img/Notification.png" class="img-circle">
                                        </#if>
                                    </div>
                                </div>

                            <div class="media-body">
                                <p class="notification-title">
                                ${invite.text}
                                <div class="notification-meta">
                                    <small class="timestamp">
                                        Time ${invite.addedDate.getHour()}:${invite.addedDate.getMinute()}

                                        day
                                    ${invite.addedDate.getDayOfMonth()} ${invite.addedDate.getMonth()} ${invite.addedDate.getYear()}
                                    </small>
                                </div>
                            </div>
                    </#if>
                </div>
            </li>
            <#else >
                You have not any notification
            </#list>
        </ul>
    </div>
</div>

<#include "include/footer.ftl">
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>