<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard subjects</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
<#include "include/header.ftl">
<#include "include/success-fade-message.ftl">
<div class="index-body">
    <div class="container admin-dashboard">
        <div class="row" style="margin-top: 2em">
            <#include "include/admin-dashboard-nav.ftl">
            <div class="col-md-9">
                <div class="panel panel-default site-overview">
                    <div class="panel-heading">
                        <h3 class="panel-title">Website Overview</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-3">
                            <div class="well dash-box">
                                <h2><span class="glyphicon glyphicon-user" aria-hidden="true"></span>${model.userListSize}</h2>
                                <h4>Users</h4>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="active well dash-box">
                                <h2><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>${model.subjectListSize}</h2>
                                <h4>Subjects</h4>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="well dash-box">
                                <h2><span class="fa fa-group" aria-hidden="true"></span>${model.groupListSize}</h2>
                                <h4>Groups</h4>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="well dash-box">
                                <h2><span class="fa fa-code" aria-hidden="true"></span>-</h2>
                                <h4>Blank</h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default site-overview">
                    <div class="panel-heading">
                        <h3 class="panel-title">Subjects</h3>
                    </div>
                    <div class="panel-body">
                        <table style="table-layout: fixed" class="table table-striped table-hover">
                            <tr>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th style="width: 85px">Delete</th>
                            </tr>
                            <#list model.subjectList as subject>
                            <tr>
                                <td><img style="width: 100%" src="${subject.image.url}" alt=""></td>
                                <td>${subject.name}</td>
                                <td>${subject.description}</td>
                                <td><a data-method="delete" data-confirm="Are you sure?" href="/admin/dashboard/subjects/delete/${subject.id}"><i class="fa fa-remove"></i></a></td>
                            </tr>
                            </#list>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<#include "include/footer.ftl">
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/main.js"></script>
</body>
</html>