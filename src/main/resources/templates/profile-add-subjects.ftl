<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add subject</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
<#include "include/header.ftl">

<div class="index-body">
    <div class="container" >
        <form action="/user/profile/subjects/add" model- method="post">
            <table class="table table-bordered table-subjects">
                <thead class="">
                <tr>
                    <th>Select</th>
                    <th>Name</th>
                    <th>Rating</th>
                    <th>Description</th>
                    <th>Image</th>
                </tr>
                </thead>
                <tbody>
                <#list subjectCardDtoWrapper.cardDtoList as subject>
                <tr>
                    <td>
                        <label class="custom-control custom-checkbox">
                            <@spring.bind path="subjectCardDtoWrapper.cardDtoList[${subject_index}].isChecked"/>
                            <input class="custom-control-input" style="display: none" type="checkbox" name="${spring.status.expression}" value="${subject.isChecked?c}" />
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td>
                    ${subject.name}
                    </td>
                    <td>
                        <label>
                            <@spring.bind path="subjectCardDtoWrapper.cardDtoList[${subject_index}].rating"/>
                            <input type="number" min="0" max="5"  value="${subject.rating}" name="${spring.status.expression}" />
                        </label>
                    </td>
                    <td>${subject.description}</td>
                    <td><img src="${subject.image.url}" alt=""></td>
                    <@spring.bind path="subjectCardDtoWrapper.cardDtoList[${subject_index}].subjectId"/>
                    <input type="hidden" name="${spring.status.expression}" value="${spring.status.value}" />
                </tr>
                </#list>
                </tbody>
                <tfoot>
                <tr>
                    <input type="hidden" name="userId" value="${auth_user.id}">
                    <th colspan="5">
                        <button type="submit" class="btn-light-blue">Save</button>
                    </th>
                </tr>
                </tfoot>
            </table>
        </form>
    </div>
</div>

<#include "include/footer.ftl">
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/main.js"></script>
</body>
</html>