<div class="col-md-3">
    <div class="list-group">
        <h3 class="list-group-item active" style="background-color: #b4b0f7; border: none; margin-top: 0"><span
                class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            Dashboard <span class="badge"></span>
        </h3>
        <a href="/admin/dashboard/users" class="list-group-item">
            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
            Users
            <span class="badge">${model.userListSize}
                        </span>
        </a>
        <a href="/admin/dashboard/subjects" class="list-group-item">
            <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
            Subjects
            <span class="badge">${model.subjectListSize}</span>
        </a>
    </div>
    <button  class=" btn btn-default group-btn-edit dropdown-toggle" type="button" data-toggle="dropdown">Create Content
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li><a href="/admin/dashboard/subjects/create">Add Subject</a></li>
        <li><a href="/admin/notification/create"">Create notification</a></li>
    </ul>
</div>