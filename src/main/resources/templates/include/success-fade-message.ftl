<#if message??>
<div id="fade-message-div" style="z-index: 1; position: absolute; width: 100%; height: 100px; text-align: center">
    <div class="alert alert-success" style="display: inline-block">
        <p>${message}</p>
    </div>
</div>
</#if>