<div class="my-navbar navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <div class="logo-div navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="logo effect-shine navbar-brand" href="/">
                <span>Dev<span class="fa fa-code" aria-hidden="true"></span>World</span>
            </a>
            <h6>MAKE IT BEFORE DEADLINE</h6>
        </div>
        <div class="navbar-collapse collapse">

            <ul class="nav navbar-nav navbar-right sign">
            <#if  (auth_user)??>
            </ul>
                <ul class="header-nav nav navbar-nav navbar-right">
                    <#if  (auth_user.role == "ADMIN")>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Admin
                            <b class="caret"></b></a>
                        <ul class="dropdown-menu">

                            <li><a href="/admin/dashboard/users">Users</a></li>
                            <li><a href="/admin/dashboard/subjects">Subjects</a></li>
                            <!--<li class="divider"></li>-->
                            <li class="dropdown-header">Management</li>
                            <li><a href="/admin/notification/create">Create notification</a></li>
                        </ul>
                    </li>
                    </#if>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Groups
                            <b class="caret"></b></a>
                        <ul class="dropdown-menu">

                            <li><a href="/user${auth_user.id}/groups">My groups</a></li>
                            <li><a href="/groups/create">Create group</a></li>
                            <!--<li class="divider"></li>-->
                            <li class="dropdown-header">On development</li>
                            <li><a href="#">Group configuration</a></li>
                            <li><a href="#">Instruction</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Profile
                            <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="/user${auth_user.id}/groups/member">Member of groups</a></li>
                            <li><a href="/user/profile/edit"">Edit profile</a></li>
                            <li><a href="/logout">Logout</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="/user/id${auth_user.id}/notifications" class="notification-bell">
                            <i data-count="${notification_count}" class="<#if notification_count != 0>notification-icon</#if> glyphicon glyphicon-bell"></i>
                        </a>
                    </li>
                    <#if  (auth_user.profile.avatar.url)??>
                        <li class="auth_img">
                            <a href="/user/id${auth_user.id}"><img src="${auth_user.profile.avatar.url}">
                            </a>
                        </li>
                    <#else>
                        <li class="auth_img">
                            <a href="/user/id${auth_user.id}"><img src="/img/you-rock.jpg">
                            </a>
                        </li>
                    </#if>
                </ul>
            <#else >
                <a type="button" data-toggle="modal" data-target="#sign-in" href="#">
                    <span>Login</span>
                </a>
                <!-- Модальное окно входа -->
                <div class="modal fade bd-example-modal-sm" id="sign-in" tabindex="-1" role="dialog"
                     aria-labelledby="in-sign-in" aria-hidden="true">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header login-modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h2 class="modal-title" id="in-sign-in">Login</h2>
                            </div>
                            <div class="modal-body login-modal-body">
                                <form action="/" method="post">
                                    <ul>
                                        <li>
                                            Login
                                            <input class="form-control reverse" name="login" type="text"
                                                   title="Логин" required/>
                                        </li>
                                        <li>
                                            Password
                                            <input class="form-control reverse" name="password" type="password"
                                                   title="Пароль" required/>
                                        </li>
                                        <li>
                                            <div class="checkbox">
                                                <label><input name="remember-me" type="checkbox"> Remember me</label>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="/register">To register account</a>
                                        </li>
                                    </ul>
                                    <hr>
                                    <div class="sign-in-buttons">

                                        <button type="button" class="gr-btn btn" data-dismiss="modal">
                                            Close
                                        </button>
                                        <button type="submit" class="ora-btn btn">Enter</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </#if>

        </div>
    </div>
</div>