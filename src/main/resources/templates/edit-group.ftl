<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Group</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
<#include "include/header.ftl">
<div class="my-body">
    <div class="container">
        <!-- Form Area -->
        <h2 class="article-title">Edit group ${group.name}</h2>
        <hr>
        <!-- Form -->
        <form method="post" action="/groups/group${group.id}/edit" enctype="multipart/form-data">
            <!-- Left Inputs -->
            <div class="col-xs-6" data-wow-delay=".5s">
                <!-- Name -->
                <input type="text" name="name" id="name" required="required" value="${group.name}" class="form" placeholder="Name"/>
                <!-- Email -->
                <input type="text" name="about" id="about" required="required" value="${group.about}" class="form" placeholder="About"/>
                <!-- Subject -->
                <#--<input type="text" name="subject" id="subject" required="required" class="form"-->
                       <#--placeholder="Subject"/>-->
                <!-- Message -->
                <textarea name="description" id="description" class="form textarea"
                          placeholder="Description">${group.description}</textarea>
            </div><!-- End Left Inputs -->
            <!-- Right Inputs -->
            <div class="col-xs-6" data-wow-delay=".5s">
                <div class="create-img-zone col-md-4" id="drop-zone">
                    <div>Just drag and drop files here
                    </div>
                    <input type="file" name="image">

                </div>
            </div>
            <!-- Bottom Submit -->
            <div class="col-xs-12">
                <button type="submit" id="submit" name="submit" class="form-btn">Save</button>
            </div>
        </form>
        <!-- End Contact Form Area -->
    </div>
</div>
<#include "include/footer.ftl">
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>