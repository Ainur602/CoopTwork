<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Groups</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
<#include "include/header.ftl">
<section class="my-body">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div style="background-color: #dfdfdf">
                <h2 style="padding-top: 20px; margin-top: 0; margin-left: 1em; font-weight: 500">Group List</h2>
                <hr>
            </div>
        <#--Modal delete confirmation window-->
            <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h2 class="modal-title" id="myModalLabel">Confirm Delete</h2>
                        </div>

                        <div class="modal-body">
                            <p>You are about to delete one track, this procedure is irreversible.</p>
                            <p>Do you want to proceed?</p>
                            <#--<p class="debug-url"></p>-->
                        </div>
                        <form action="/groups/delete" method="post">
                            <div class="modal-footer">
                                <input type="hidden" id="delete-group-id" name="groupId" value="0">
                                <button type="button" style="float: right" class="group-btn-edit" data-dismiss="modal">Cancel</button>
                                <button type="submit" style="float: right; margin-right: 1em" class="group-btn-del  btn-ok">Delete</button>

                                <#--<a class="btn btn-danger btn-ok">Delete</a>-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <#--Modal window end-->
            <div class="col-xs-8" style="margin-left: 0">
                <ul class="group-list" id="searchList">
                    <#list model.groupList as group>
                    <li class="s-list">
                        <a href="/groups/group${group.id}">
                            <img alt="${group.image.originalFileName}"
                                 src="${group.image.url}"/>
                        </a>
                        <div class="info">
                            <a  href="/groups/group${group.id}">
                                <h3 class="search-name title">${group.name}</h3>
                            </a>
                            <p class="desc">${group.about}</p>
                        </div>
                        <#if auth_user.id == group.employer.id>
                            <div class="social">
                                <ul>
                                    <li class="i-edit" style="width:33%;"><a href="/groups/group${group.id}/edit"><span
                                            class="fa fa-cog"></span></a></li>
                                    <li class="i-group" style="width:34%;">
                                        <a href="/groups/group${group.id}/members"><span
                                            class="fa fa-users"></span></a>
                                    </li>

                                    <li class="i-trash" style="width:33%;">
                                        <a   href="#" data-href="${group.id}" data-toggle="modal" data-target="#confirm-delete"><span class="fa fa-trash"></span></a>
                                    </li>
                                </ul>
                            </div>
                        </#if>
                    </li>
                    <#else >
                        Empty
                    </#list>
                </ul>
            </div>
            <div class="col-xs-3 group-page-filter">
                <div class="input-group stylish-input-group">
                    <input type="text" onkeyup="myFunction()" id="myInput" class="form-control" placeholder="Search">
                    <span class="input-group-addon">
                        <button type="submit">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
                <div class="group-page-filter-block">
                    <ul >
                        <#--<li ><button onclick="sortList()"> FILTER A-Z</button></li>-->
                        <li><a onclick="return sortList()" onmouseover="" style="cursor: pointer">Sort A-Z</a></li>
                        <li><a onclick="return sortListDesc()" onmouseover="" style="cursor: pointer">Sort Z-A</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<#include "include/footer.ftl">
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script type="text/javascript">
    function myFunction() {
        var input, filter, ul, li, a, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        ul = document.getElementById("searchList");
        li = ul.getElementsByClassName("s-list");
        console.log(li.length);
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByClassName("search-name");
            if (a[0].innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }


    function sortList() {
        var list, i, switching, b, shouldSwitch;
        list = document.getElementById("searchList");
        switching = true;
        /*Make a loop that will continue until
        no switching has been done:*/
        while (switching) {
            //start by saying: no switching is done:
            switching = false;
            b = list.getElementsByClassName("s-list");
            //Loop through all list-items:
            for (i = 0; i < (b.length - 1); i++) {
                //start by saying there should be no switching:
                shouldSwitch = false;
                /*check if the next item should
                switch place with the current item:*/
                if (b[i].getElementsByClassName("search-name")[0].innerHTML.toLowerCase() > b[i + 1].getElementsByClassName("search-name")[0].innerHTML.toLowerCase()) {
                    /*if next item is alphabetically
                    lower than current item, mark as a switch
                    and break the loop:*/
                    shouldSwitch = true;
                    break;
                }
            }
            if (shouldSwitch) {
                /*If a switch has been marked, make the switch
                and mark the switch as done:*/
                b[i].parentNode.insertBefore(b[i + 1], b[i]);
                switching = true;
            }
        }
    }
    function sortListDesc() {
        var list, i, switching, b, shouldSwitch;
        list = document.getElementById("searchList");
        switching = true;
        /*Make a loop that will continue until
        no switching has been done:*/
        while (switching) {
            //start by saying: no switching is done:
            switching = false;
            b = list.getElementsByClassName("s-list");
            //Loop through all list-items:
            for (i = 0; i < (b.length - 1); i++) {
                //start by saying there should be no switching:
                shouldSwitch = false;
                /*check if the next item should
                switch place with the current item:*/
                if (b[i].getElementsByClassName("search-name")[0].innerHTML.toLowerCase() < b[i + 1].getElementsByClassName("search-name")[0].innerHTML.toLowerCase()) {
                    /*if next item is alphabetically
                    lower than current item, mark as a switch
                    and break the loop:*/
                    shouldSwitch = true;
                    break;
                }
            }
            if (shouldSwitch) {
                /*If a switch has been marked, make the switch
                and mark the switch as done:*/
                b[i].parentNode.insertBefore(b[i + 1], b[i]);
                switching = true;
            }
        }
    }

    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        document.getElementById("delete-group-id").value = $(this).find('.btn-ok').attr('href');
        $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
    });
</script>
</body>
</html>