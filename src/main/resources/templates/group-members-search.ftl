<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${group.name} Team</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
<#include "include/header.ftl">

<div class="my-body index-body">
    <div class="container users-team-body">
        <h2 style="color: #333333" class="article-title">${group.name} Team</h2>
        <div style="margin-top: 2em" class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div>
                        <table class="table table-filter">
                            <tbody>
                            <#list userList as user>

                                <#if group.employer.id != user.id>

                                <tr data-status="pendiente">

                                    <td>
                                        <div class="media">
                                            <a href="/user/id${user.id}" class="pull-left">
                                                <#if (user.profile.avatar.url)??>
                                                    <img src="${user.profile.avatar.url}"
                                                         class="media-photo">
                                                <#else >
                                                    <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg"
                                                         class="media-photo">
                                                </#if>
                                            </a>
                                            <div class="media-body">
                                                <div style="margin-top: 7px" class=" pull-right">
                                                    <form action="/groups/group${group.id}/members/add" method="post">
                                                        <input type="hidden" name="employeeId" value="${user.id}">
                                                        Add to the group
                                                        <button type="submit"
                                                                style="padding: 3px 15px 3px 15px; height: auto"
                                                                class="btn-blue"><i class="fa fa-plus"></i></button>

                                                    </form>
                                                </div>
                                                <h4 class="title">
                                                ${user.profile.name} ${user.profile.name}(${user.login})
                                                <#--<span class="pull-right pendiente">(Pendiente)</span>-->
                                                </h4>
                                                <p class="summary">
                                                    <#if (user.profile.about)??>
                                                    ${user.profile.about}
                                                    <#else >
                                                        Unknown
                                                    </#if>
                                                </p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </#if>
                            </#list>
                            </tbody>
                        </table>
                        <div class="search-container">
                            <form action="/groups/group${group.id}/members/search" method="get">
                                <input type="text" placeholder="Search.." name="login">
                                <button style="height: 33px" class="btn-blue" type="submit">Invite</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<#include "include/footer.ftl">
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>