<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Subjects</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
<#include "include/header.ftl">

<div class="index-body">

    <div class="container">
        <div class="profile-div">
            <ul class="navigation">
                <li>
                    <a href="/user/id${auth_user.id}">
                        <span class="glyphicon glyphicon-user"></span>
                    </a>
                </li>
                <li class="active">
                    <a href="">
                        <span class="glyphicon glyphicon-list-alt"></span>
                    </a>
                </li>
                <li>
                    <a href="/user/profile/edit"">
                        <span class="glyphicon glyphicon-cog"></span>
                    </a>
                </li>

            </ul>
            <div class="user-body">
                <div class="tab-content subject">
                    <#list model.subjects as association>
                    <div class="col-lg-4">
                        <div class="card">
                            <img src="${association.subject.image.url}">
                            <h4>${association.subject.name}</h4>
                            <p>How do you know Java Core</p>
                            <div class=" lead">
                                <div id="hearts-existing" class="starrr" data-rating='${association.rating}'></div>
                            </div>
                            <a href="#" class="sub-card-del-btn">Delete <i class="glyphicon glyphicon-trash"></i></a>
                        </div>
                    </div>
                    <#else>
                        You have not subjects yet
                    </#list>

                    <div class="col-xs-12 group-btns">
                        <!-- Send Button -->
                        <a href="/user/subjects${auth_user.id}/add">
                            <button type="button" class="group-btn-edit">+ ADD</button>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<#include "include/footer.ftl">
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/main.js"></script>
</body>
</html>