<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile page</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>

<#include "include/header.ftl">

<div class="index-body">

    <div class="container">
        <div class="profile-div">
            <ul class="navigation">
                <li class="active">
                    <a href="">
                        <span class="glyphicon glyphicon-user"></span>
                    </a>
                </li>
                <li>
                    <a  href="/user/subjects${auth_user.id}">
                        <span class="glyphicon glyphicon-list-alt"></span>
                    </a >
                </li>
                <li>
                    <a href="/user/profile/edit">
                        <span class="glyphicon glyphicon-cog"></span>
                    </a>
                </li>

            </ul>
            <div class="user-body">
                <div class=" row tab-pane active">
                    <div class="col-md-4 profile-img">

                    <#if (model.user.profile.avatar.url)??>

                        <img src="${model.user.profile.avatar.url}" alt="">
                    <#else >
                        <img src="/img/you-rock.jpg">
                    </#if>
                    </div>
                    <div class="col-md-8">
                        <h2>Profile information</h2>
                        <hr>
                        <div style="margin-right: 1em" class="row panel panel-default">
                            <div class="col-sm-3">
                                <h3>Login:</h3>
                                <h3>First name:</h3>
                                <h3>Last name:</h3>
                                <h3>Email:</h3>
                                <h3>Birthday:</h3>
                                <h3>About:</h3>
                                <h3>Skills:</h3>

                            </div>
                            <div class="col-sm-9">
                                <h3>${model.user.login}</h3>
                                <h3>
                                    <#if (model.user.profile.name)??>
                                        ${model.user.profile.name}
                                    <#else >
                                        Steve
                                    </#if>
                                </h3>
                                <h3>
                                <#if (model.user.profile.surname)??>
                                    ${model.user.profile.surname}
                                <#else >
                                    Jobs
                                </#if>
                                </h3>
                                <h3>${model.user.email}</h3>
                                <h3>
                                <#if (model.user.profile.birthday)??>
                                    ${model.user.profile.birthday}
                                <#else >
                                    Unknown
                                </#if>
                                </h3>
                                <h3>
                                    <#if (model.user.profile.about)??>
                                    ${model.user.profile.about}
                                    <#else >
                                        Unknown
                                    </#if>
                                </h3>
                                <h3>
                                    <#list model.user.profile.subjects as subject>

                                    <span class="tags">${subject.subject.name}</span>
                                    <#else >
                                        Empty
                                    </#list>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<#include "include/footer.ftl">

<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/main.js"></script>

</body>
</html>