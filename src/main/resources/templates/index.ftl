<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Index</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
<#include "include/header.ftl">
<#include "include/success-fade-message.ftl">
<#if error??>
    <div style="margin: 0; text-align: center" class="alert alert-danger" role="alert">
        The username or password are not correct
    </div>
</#if>
<div class="index-body">
    <div class="container">
        <div style="margin-top: 5em" class="index-content text-center">
            <h2>For developers</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            </p>
            <p>
                Maecenas lacus libero, convallis in sollicitudin ornare, accumsan ut urna. Donec ac varius nisl. Quisque
                auctor accumsan orci eget rhoncus. Vivamus sagittis, est in placerat fringilla, est lacus eleifend
                libero, ut placerat orci nisl sed neque.</p>
            <img src="img/index.png" alt="Index image">
        </div>
    </div>
</div>

<#include "include/footer.ftl">
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script>
    $('#fade-message-div').fadeIn('slow').delay(4000).fadeOut('slow');
</script>
</body>
</html>