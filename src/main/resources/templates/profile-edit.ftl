<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Setting Profile</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>

<#include "include/header.ftl">
<#if error??>
<div style="margin: 0; text-align: center" class="alert alert-danger" role="alert">${error}</div>
</#if>
<div class="index-body">
    <div class="container">
        <div class="profile-div">
            <ul class="navigation">
                <li>
                    <a href="/user/id${auth_user.id}">
                        <span class="glyphicon glyphicon-user"></span>
                    </a>
                </li>
                <li>
                    <a href="/user/subjects${auth_user.id}">
                        <span class="glyphicon glyphicon-list-alt"></span>
                    </a>
                </li>
                <li class="active">
                    <a href="">
                        <span class="glyphicon glyphicon-cog"></span>
                    </a>
                </li>

            </ul>
            <div class="user-body">

                <form action="/user/profile/edit" class="row" method="post" enctype="multipart/form-data">
                    <div class="upload-drop-zone col-md-4" id="drop-zone">
                        <div>Just drag and drop files here
                        </div>
                        <input type="file" name="avatar">
                    </div>
                    <div class="row col-md-8">
                        <h2>Settings</h2>
                        <hr>
                        <div class="panel panel-default">
                            <div class="panel-body form-horizontal payment-form">
                                <div class="form-group">
                                    <label for="First_name" class="col-sm-3 control-label">First name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="First_name" name="name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Last_name" class="col-sm-3 control-label">Last name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="Last_name" name="surname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="date" class="col-sm-3 control-label">Date</label>
                                    <div class="col-sm-9">
                                        <input type="date" class="form-control" id="date" name="birthday">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="about" class="col-sm-3 control-label">About</label>
                                    <div class="col-sm-9">
                                        <textarea name="about" id="about" class="form-control" placeholder="Something about yourself :)"></textarea>
                                    </div>
                                </div>
                                <#--<div class="form-group">-->
                                    <#--<label for="email" class="col-sm-3 control-label">Email</label>-->
                                    <#--<div class="col-sm-9">-->
                                        <#--<input type="email" class="form-control" id="email" name="email">-->
                                    <#--</div>-->
                                <#--</div>-->
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 text-right">
                                <button style="float: right" type="submit" class=" btn btn-default group-btn-edit ">
                                    <span class="glyphicon glyphicon-saved"></span> Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<#include "include/footer.ftl">
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/main.js"></script>
</body>
</html>